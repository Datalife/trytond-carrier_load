==============
Carrier load
==============

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from proteus import Model, Wizard, Report
    >>> today = datetime.date.today()

Install agro Module::

    >>> config = activate_modules('carrier_load')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> Uom = Model.get('product.uom')
    >>> Template = Model.get('product.template')
    >>> carrier = Carrier()
    >>> unit, = Uom.find([('name', '=', 'Unit')], limit=1)
    >>> transport_template = Template(
    ...     name='Transport',
    ...     type='service',
    ...     list_price=Decimal(500),
    ...     cost_price=Decimal(0),
    ...     default_uom=unit)
    >>> transport_template.save()
    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = transport_template.products[0]
    >>> carrier.save()
    >>> carrier_product = carrier.carrier_product.template
    >>> carrier_product.purchasable = True
    >>> carrier_product.purchase_uom = carrier_product.default_uom
    >>> carrier_product.account_category = account_category_tax
    >>> carrier_product.save()

Get warehouse and dock::

    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> dock = wh.docks.new()
    >>> dock.name = 'Dock 1'
    >>> dock.code = 'D1'
    >>> wh.save()
    >>> storage, = Location.find([
    ...     ('type', '=', 'storage'),
    ...     ('name','=','Storage Zone')], limit=1)

Create carrier load::

    >>> Load = Model.get('carrier.load')
    >>> load = Load()
    >>> load.company != None
    True
    >>> load.state
    'draft'
    >>> load.date == today
    True
    >>> load.warehouse != None
    True
    >>> load.warehouse_output == load.warehouse.output_location
    True
    >>> load.dock != None
    True
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX3449'
    >>> load.save()
    >>> load.code != None
    True
    >>> load.driver = 'Driver'
    >>> load.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.RequiredValidationError: A value is required for field "Driver identifier" in "Carrier load". - 
    >>> load.driver_identifier = 'ID-Driver'
    >>> load.driver_phone = '+34968057451'
    >>> load.save()

Create load order::

    >>> Order = Model.get('carrier.load.order')
    >>> order = Order(load=load)
    >>> order.state
    'draft'
    >>> order.party = customer
    >>> line = order.lines.new()
    >>> order.save()
    >>> order.code != None
    True
    >>> order.date == load.date
    True
    >>> order.type = 'internal'
    >>> order.storage_locations != None
    True
    >>> set([location.type for location in order.storage_locations])
    {'storage'}
    >>> order.to_location = wh
    >>> order.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelstorage.DomainValidationError: The value for field "To location" in "Carrier load order" is not valid according to its domain. - 
    >>> order.to_location = storage
    >>> order.save()
    >>> order.type = 'out'
    >>> order.storage_locations == []
    True
    >>> order.click('wait')
    >>> order.state
    'waiting'

Define incoterms::

    >>> IncotermRule = Model.get('incoterm.rule')
    >>> fob_rule, = IncotermRule.find([('abbreviation', '=', 'FOB')])
    >>> order.incoterm_version != None
    True
    >>> incoterm = order.incoterms.new()
    >>> incoterm.rule = fob_rule
    >>> incoterm.place = 'Hamburg'
    >>> order.save()

Confirm load::

    >>> load.click('confirm')
    >>> load.reload()
    >>> load.state
    'confirmed'

Create purchase::

    >>> bool(load.purchasable)
    False
    >>> load.click('create_purchase')
    >>> load.reload()
    >>> not load.purchase
    True
    >>> load.purchasable = True
    >>> load.save()
    >>> load.currency != None
    True
    >>> load.click('create_purchase')
    >>> not load.purchase
    True
    >>> load.unit_price = Decimal(300.0)
    >>> load.save()
    >>> not load.purchase_state
    True
    >>> load.click('do')
    >>> load.reload()
    >>> load.state
    'done'
    >>> load.purchase != None
    True
    >>> load.purchase_state
    'quotation'
    >>> order.reload()
    >>> order.carrier_amount
    Decimal('300.00')

Set load to confirmed from done state::

    >>> load.click('confirm') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> purchase = load.purchase
    >>> purchase.click('draft')
    >>> purchase.delete()
    >>> load.reload()
    >>> load.click('confirm')
    >>> load.reload()
    >>> load.state
    'confirmed'

Check reports::

    >>> load_sheet = Report('carrier.load.sheet')
    >>> ext, _, _, name = load_sheet.execute([load], {})
    >>> ext
    'odt'
    >>> name
    'Load sheet-1'
    >>> cmr = Report('carrier.load.order.cmr')
    >>> ext, _, _, name = cmr.execute([load.orders[0]], {})
    >>> ext
    'odt'
    >>> name
    'CMR-1'
    >>> road_note = Report('carrier.load.order.road_note')
    >>> ext, _, _, name = road_note.execute([load.orders[0]], {})
    >>> ext
    'odt'
    >>> name
    'Road transport note-1'

Check wizard purchase carrier load::

    >>> wizard = Wizard('carrier.load.print_purchase', [load])
    >>> ext, _, _, name = wizard.actions[0]
    >>> ext
    'odt'
    >>> name
    'Carrier load purchase'

Check carrier configuration states::

    >>> Conf = Model.get('carrier.configuration')

    >>> conf = Conf(1)
    >>> conf.shipment_internal_state
    'done'
    >>> conf.shipment_out_state
    'packed'
    >>> conf.sale_state
    'quotation'
    >>> conf.shipment_internal_state = 'draft'
    >>> conf.shipment_out_state = 'draft'
    >>> conf.sale_state = 'draft'
    >>> conf.save()
    >>> conf.shipment_internal_state
    'draft'
    >>> conf.shipment_out_state
    'draft'
    >>> conf.sale_state
    'draft'
     >>> conf.shipment_internal_state = 'done'
    >>> conf.shipment_out_state = 'packed'
    >>> conf.sale_state = 'quotation'
    >>> conf.save()

Check cmr instructions::

    >>> Order = Model.get('carrier.load.order')
    >>> Conf = Model.get('carrier.configuration')
    >>> CMRTemplate = Model.get('carrier.load.cmr.template')
    >>> cmr_template = CMRTemplate(name='default')
    >>> cmr_tline = cmr_template.lines.new()
    >>> cmr_tline.section = '13'
    >>> cmr_tline.text = 'global CMR instructions'
    >>> cmr_template.save()
    >>> carrier_conf = Conf(1)
    >>> carrier_conf.cmr_template = cmr_template
    >>> carrier_conf.save()
    >>> order = Order(load=load)
    >>> order.save()
    >>> order.cmr_instructions
    'global CMR instructions'
    >>> order.party = customer
    >>> order.save()
    >>> cmr_template2 = CMRTemplate(name='Customer template')
    >>> cmr_tline = cmr_template2.lines.new()
    >>> cmr_tline.section = '13'
    >>> cmr_tline.text = 'Party instructions'
    >>> cmr_template2.save()    
    >>> customer.cmr_template = cmr_template2
    >>> customer.save()
    >>> order.cmr_instructions
    'Party instructions'
