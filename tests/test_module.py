# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class CarrierLoadTestCase(ModuleTestCase):
    """Test carrier load module"""
    module = 'carrier_load'
    extras = ['sale_cost_apply_invoice']

    def setUp(self):
        super().setUp()


del ModuleTestCase
