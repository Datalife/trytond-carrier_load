============================
Carrier load Carrier vehicle
============================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from proteus import Model, Wizard, Report
    >>> today = datetime.date.today()

Install agro Module::

    >>> config = activate_modules(['carrier_load', 'carrier_vehicle'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()


Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> Uom = Model.get('product.uom')
    >>> Template = Model.get('product.template')
    >>> carrier = Carrier()
    >>> unit, = Uom.find([('name', '=', 'Unit')], limit=1)
    >>> transport_template = Template(
    ...     name='Transport',
    ...     type='service',
    ...     list_price=Decimal(500),
    ...     cost_price=Decimal(0),
    ...     default_uom=unit)
    >>> transport_template.save()
    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = transport_template.products[0]
    >>> carrier.save()
    >>> carrier_product = carrier.carrier_product.template
    >>> carrier_product.purchasable = True
    >>> carrier_product.purchase_uom = carrier_product.default_uom
    >>> carrier_product.save()


Create vehicle and type::

    >>> VehicleType = Model.get('carrier.vehicle.type')
    >>> Vehicle = Model.get('carrier.vehicle')
    >>> vehicle_type = VehicleType()
    >>> vehicle_type.name = 'vehicle type'
    >>> vehicle_type.save()
    >>> vehicle = Vehicle()
    >>> vehicle.carrier = carrier
    >>> vehicle.driver = 'Driver'
    >>> vehicle.driver_identifier = 'D10'
    >>> vehicle.number = 'MX1234'
    >>> vehicle.trailer_number = 'AX4567'
    >>> vehicle.type = vehicle_type
    >>> vehicle.save()


Create carrier load::

    >>> Load = Model.get('carrier.load')
    >>> load = Load()
    >>> load.carrier = carrier
    >>> load.vehicle_number == None
    True
    >>> load.trailer_number == None
    True
    >>> load.driver == None
    True
    >>> load.driver_identifier == None
    True
    >>> load.vehicle = vehicle
    >>> load.vehicle_number == vehicle.number
    True
    >>> load.trailer_number == vehicle.trailer_number
    True
    >>> load.driver == vehicle.driver
    True
    >>> load.driver_identifier == vehicle.driver_identifier
    True
    >>> load.save()
