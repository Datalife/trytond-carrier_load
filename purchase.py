# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    loads = fields.One2Many('carrier.load', 'purchase', 'Loads',
        readonly=True)
